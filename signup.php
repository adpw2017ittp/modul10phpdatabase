<?php require('connection.php');?>
<?php require('cek_session_login.php');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Pendaftaran</title>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jQuery-2.1.4.min.js"></script>
	<!-- <script type="text/javascript" src="assets/js/bootstrap.js"></script> -->
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body class="bg-default" style="background-color:#fafafa">
	<?php include('navbar.php');?>
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-md-4">
	      1 of 3
	    </div>
	    <div class="col-md-4">
	      2 of 3
	    </div>
	    <div class="col-md-4">
	    	<div class="card" style="margin-top:1rem;margin-bottom:1rem">
				  <div class="card-body">
				  	<div class="text-center">
					    <h4 class="card-title text-primary">Buat Akun Baru</h4>
					    <p class="card-text">belum punya akun? buat segera di sini!</p>
				  	</div><br>
				    <form method="post" action="signup_process.php">
						  <div class="form-group">
						    <label for="nama">Nama Lengkap</label>
						    <input name="fullname" type="text" class="form-control" id="nama" required="">
						  </div>
						  <div class="form-group">
						    <label for="email">Email</label>
						    <input name="email" type="email" class="form-control" id="email" required="">
						  </div>
						  <div class="form-group">
						    <label for="username">Username</label>
						    <input name="username" type="text" class="form-control" id="username" required="">
						  </div>
						  <div class="form-group">
						    <label for="password">Password</label>
						    <input name="password" type="password" class="form-control" id="password" required="">
						  </div>
						  <div class="form-group">
						    <label for="role">Role</label>
						    <input name="role" type="text" class="form-control" id="role" required="">
						  </div>
						  <div class="form-check">
							  <input class="form-check-input" type="checkbox" value="" id="sdkb" required="">
							  <label class="form-check-label" for="sdkb">
							    Dengan ini saya menyetujui <a href="#">syarat dan ketentuan</a> yang berlaku
							  </label>
							</div>
							<br>
				    	<button class="btn btn-primary" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;BUAT&nbsp;&nbsp;&nbsp;&nbsp;</button>
				    </form>
				  </div>
				  <div class="card-footer text-muted">
				    <small>@copyright. Informatika IT Telkom Purwokerto</small>
				  </div>
				</div>
	    </div>
	  </div>
	</div>

</body>
</html>
