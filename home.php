<?php require('connection.php');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Masuk</title>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jQuery-2.1.4.min.js"></script>
	<!-- <script type="text/javascript" src="assets/js/bootstrap.js"></script> -->
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body class="bg-default" style="background-color:#fafafa">
	<?php include('navbar.php');?>
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-md-2">
	      1 of 3
	    </div>
	    <div class="col-md-8">
	    	<div class="card" style="margin-top:1rem;margin-bottom:1rem">
				  <div class="card-body">
					    <h4 class="card-title text-primary text-center">Selamat Datang di Website Kami</h4><br>
					    <p class="card-text text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					    <br>
					    <div class="text-center">
					    	<a class="btn btn-primary" href="signup.php" style="margin-right:1rem">Daftar</a>
					    	<a class="btn btn-success" href="signin.php">Masuk</a>
					    </div>
					    <br>
				  </div>
				  <div class="card-footer text-muted">
				    <small>@copyright. Informatika IT Telkom Purwokerto</small>
				  </div>
				</div>
	    </div>
	    <div class="col-md-2">
	      3 of 3
	    </div>
	  </div>
	</div>

</body>
</html>
