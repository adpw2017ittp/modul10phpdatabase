<?php require('connection.php');?>
<?php require('cek_session_login.php');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Masuk</title>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jQuery-2.1.4.min.js"></script>
	<!-- <script type="text/javascript" src="assets/js/bootstrap.js"></script> -->
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body class="bg-default" style="background-color:#fafafa">
	<?php include('navbar.php');?>
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-md-4">
	      1 of 3
	    </div>
	    <div class="col-md-4">
	      2 of 3
	    </div>
	    <div class="col-md-4">
	    	<div class="card" style="margin-top:1rem;margin-bottom:1rem">
				  <div class="card-body">
				  	<div class="text-center">
					    <h4 class="card-title text-success">Masuk</h4>
					    <p class="card-text">kami menunggu anda</p>
				  	</div><br>
				    <form method="post" action="signin_process.php">
						  <div class="form-group">
						    <label for="username">Username</label>
						    <input name="username" type="text" class="form-control" id="username">
						  </div>
						  <div class="form-group">
						    <label for="password">Password</label>
						    <input name="password" type="password" class="form-control" id="password">
						  </div>
							<br>
				    	<button class="btn btn-success" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Masuk&nbsp;&nbsp;&nbsp;&nbsp;</button>
				    </form>
				  </div>
				  <div class="card-footer text-muted">
				    <small>@copyright. Informatika IT Telkom Purwokerto</small>
				  </div>
				</div>
	    </div>
	  </div>
	</div>

</body>
</html>
