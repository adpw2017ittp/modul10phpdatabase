<?php require('connection.php');?>
<?php require('cek_session_admin.php');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Masuk</title>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jQuery-2.1.4.min.js"></script>
	<!-- <script type="text/javascript" src="assets/js/bootstrap.js"></script> -->
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body class="bg-default" style="background-color:#fafafa">
	<?php include('navbar_admin.php');?>
	<div class="container-fluid">
	  <div class="row">
	  	<div class="col-md-2"></div>
	    <div class="col-md-8">
	    	<div style="padding:1rem">
		    	<div class="card">
		    		<div class="card-body">
				    	<a class="btn btn-primary float-md-right" href="add_user.php">Tambah User</a>
				    	<h3>Daftar User</h3>
				    	<table class="table table-striped bg-white table-hover">
							  <thead>
							    <tr class="bg-primary text-white">
							      <th scope="col">No</th>
							      <th scope="col">Nama Lengkap</th>
							      <th scope="col">Username</th>
							      <th scope="col">Email</th>
							      <th scope="col">Role</th>
							      <th scope="col">aksi</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <th scope="row">1</th>
							      <td>John Doe</td>
							      <td>john</td>
							      <td>john@email.com</td>
							      <td>admin</td>
							      <td>
							      	<a href="edit.php">edit</a> |
							      	<a class="text-danger" href="delete.php">hapus</a>
							      </td>
							    </tr>
							    <tr>
							      <th scope="row">2</th>
							      <td>Mufti Robbani</td>
							      <td>mufti3008</td>
							      <td>mufti3008@email.com</td>
							      <td>user</td>
							      <td>
							      	<a href="edit.php">edit</a> |
							      	<a class="text-danger" href="delete.php">hapus</a>
							      </td>
							    </tr>
							    <tr>
							      <th scope="row">3</th>
							      <td>Bagus Rachmad Hartanto Prabowo</td>
							      <td>baguzzza</td>
							      <td>baguzzza@email.com</td>
							      <td>user</td>
							      <td>
							      	<a href="edit.php">edit</a> |
							      	<a class="text-danger" href="delete.php">hapus</a>
							      </td>
							    </tr>
							  </tbody>
							</table>
						</div>
					</div>
	    	</div>
	    </div>
	    </div>
	  </div>
	</div>

</body>
</html>
